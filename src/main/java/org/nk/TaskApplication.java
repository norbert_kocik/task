package org.nk;

import io.dropwizard.Application;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.FileConfigurationSourceProvider;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.nk.endpoint.TaskEndpointResource;
import org.nk.repository.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by b on 03.05.15.
 */
public class TaskApplication extends Application<TaskApplicationConfiguration> {

    public static void main(String[] args) throws Exception {
        new TaskApplication().run(args);
    }

    @Override
    public void initialize(Bootstrap<TaskApplicationConfiguration> bootstrap) {
        bootstrap.setConfigurationSourceProvider(
                new SubstitutingSourceProvider(bootstrap.getConfigurationSourceProvider(),
                        new EnvironmentVariableSubstitutor()
                )
        );
    }

    protected EntryRepository createEntryRepository(TaskApplicationConfiguration configuration) throws MalformedURLException {
        List<EntryRepository> repositories=new ArrayList<EntryRepository>();
        for(String rssURL:configuration.getRssFeedURLs()) {
            repositories.add(new RssFeedRepository(new URL(rssURL)));
        }
        for(String restURL:configuration.getJsonFeedURLs()) {
            repositories.add(new RESTFeedRepository(restURL));
        }
        return new MergingFeedsRepository(repositories, ByKeyExtractionSameEntriesGrouper.ByTitleGrouper,
                new MergingFeedsRepository.GetFirstMergingStrategy());
    }

    @Override
    public void run(TaskApplicationConfiguration configuration, Environment environment) throws Exception {

        environment.jersey().register(new TaskEndpointResource(createEntryRepository(configuration)));

    }
}
