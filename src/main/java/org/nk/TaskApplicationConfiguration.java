package org.nk;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by b on 03.05.15.
 */
public class TaskApplicationConfiguration extends Configuration {

    private String[] rssFeedURLs ;

    private String[] jsonFeedURLs ;

    public String[] getRssFeedURLs() {
        return rssFeedURLs;
    }

    public void setRssFeedURLs(String[] rssFeedURLs) {
        this.rssFeedURLs = rssFeedURLs;
    }

    public String[] getJsonFeedURLs() {
        return jsonFeedURLs;
    }

    public void setJsonFeedURLs(String[] jsonFeedURLs) {
        this.jsonFeedURLs = jsonFeedURLs;
    }
}
