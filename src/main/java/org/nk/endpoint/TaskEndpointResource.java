package org.nk.endpoint;

import com.google.common.base.Optional;
import org.nk.model.Entry;
import org.nk.model.EntriesFilter;
import org.nk.repository.EntryRepository;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.Arrays;
import java.util.Collection;

/**
 * Created by b on 03.05.15.
 */
@Path("/entries")
@Produces(MediaType.APPLICATION_JSON)
public class TaskEndpointResource {

    private EntryRepository entryRepository;

    public TaskEndpointResource(EntryRepository entryRepository) {
        this.entryRepository = entryRepository;
    }

    @GET
    public Entry[] entries(@QueryParam("category") Optional<String> category) {
        Collection<Entry> entries= Arrays.asList(entryRepository.getEntries());
        // could move filter down to getEntries
        EntriesFilter filter=new EntriesFilter(category);
        entries=filter.apply(entries);
        return entries.toArray(new Entry[entries.size()]);
    }
}
