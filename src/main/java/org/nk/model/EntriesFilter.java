package org.nk.model;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;

import javax.annotation.Nullable;
import java.util.Collection;

/**
 * Created by b on 03.05.15.
 */
public class EntriesFilter implements Function<Collection<Entry>,Collection<Entry>> {

    private String category;

    public EntriesFilter(Optional<String> category) {
        if(category.isPresent()) {
            this.category = category.get();
        }
    }

    private Collection<Entry> filterByCategory(Collection<Entry> entries,final String category) {
        return Collections2.filter(entries, new Predicate<Entry>() {
            public boolean apply(Entry entry) {
                return category.equals(entry.getCategory());
            }
        });
    }

    public Collection<Entry> apply(Collection<Entry> entries) {
        if(category!=null) {
            entries=filterByCategory(entries,category);
        }
        return entries;
    }
}
