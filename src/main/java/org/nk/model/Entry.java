package org.nk.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.Chronology;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by b on 03.05.15.
 */
public class Entry {



    private String title;
    private String link;
    private String description;
    private String date;
    private String time;
    private String category;
    //from rss feed
    //guid
    //vg:waplink
    //vg:img
    //vg:articleImg
    //image
    //body
    // not indented, at this point or I would have to add getters to make bean serialization in XStream;
    // or I would have to change XStream, or add intermmidiate class/conversion
    @Deprecated
    private String pubDate=null;

    @JsonCreator
    public Entry(@JsonProperty("title") String title,
                 @JsonProperty("link") String link,
                 @JsonProperty("description") String description,
                 @JsonProperty("date") String date,
                 @JsonProperty("time") String time,
                 @JsonProperty("category") String category) {
        this.title = title;
        this.link = link;
        this.description = description;
        this.date = date;
        this.time = time;
        this.category = category;
    }
    @JsonProperty
    public String getTitle() {
        return title;
    }
    @JsonProperty
    public String getLink() {
        return link;
    }
    @JsonProperty
    public String getDescription() {
        return description;
    }

    @JsonProperty
    public String getDate() {
        if(pubDate!=null) {
            return EntryHelper.convertPubDateToDate(pubDate);
        }
        return date;
    }
    @JsonProperty
    public String getTime() {
        if(pubDate!=null) {
            return EntryHelper.convertPubDateToTime(pubDate);
        }
        return time;
    }
    @JsonProperty
    public String getCategory() {
        return category;
    }


}
