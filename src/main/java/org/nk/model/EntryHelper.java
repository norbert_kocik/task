package org.nk.model;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Locale;

/**
 * Created by b on 03.05.15.
 */
public class EntryHelper {

    public static DateTimeFormatter PubDateFormatter= DateTimeFormat.forPattern("E, dd MMMM yyyy HH:mm:ss Z").withLocale(Locale.ENGLISH);
    public static DateTimeFormatter DateFormatter=DateTimeFormat.forPattern("dd MMMM yyyy").withLocale(Locale.ENGLISH);
    public static DateTimeFormatter TimeFormatter=DateTimeFormat.forPattern("HH:mm").withLocale(Locale.ENGLISH);

    public static String convertPubDateToTime(String pubDate) {
        //will change to UTC but probably this is not what we want
        DateTime dateTime=PubDateFormatter.parseDateTime(pubDate).toDateTime(DateTimeZone.UTC);
        return TimeFormatter.print(dateTime);
    }

    public static String convertPubDateToDate(String pubDate) {
        //will change to UTC but probably this is not what we want
        DateTime dateTime=PubDateFormatter.parseDateTime(pubDate).toDateTime(DateTimeZone.UTC);
        return DateFormatter.print(dateTime);
    }
}
