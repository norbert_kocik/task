package org.nk.repository;

import com.google.common.base.Function;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import org.nk.model.Entry;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by b on 03.05.15.
 */
public class ByKeyExtractionSameEntriesGrouper<Key> implements MergingFeedsRepository.SameEntriesGrouper {

    private Function<Entry,Key> keyMapper;

    public static final ByKeyExtractionSameEntriesGrouper ByTitleGrouper = new ByKeyExtractionSameEntriesGrouper<String>(new Function<Entry, String>() {
        public String apply(Entry entry) {
            return entry.getTitle();
        }
    });

    public ByKeyExtractionSameEntriesGrouper(Function<Entry, Key> keyMapper) {
        this.keyMapper = keyMapper;
    }

    public Collection<Collection<Entry>> apply(Collection<Entry> entries) {
        Multimap<Key,Entry> ret= ArrayListMultimap.create(entries.size(),1);
        for(Entry entry:entries) {
            // technically we should check for keys which were not created, ignoring
            ret.put(keyMapper.apply(entry),entry);
        }
        return ret.asMap().values();
    }
}
