package org.nk.repository;

import org.nk.model.Entry;

/**
 * Created by b on 03.05.15.
 */
public interface EntryRepository {
    Entry[] getEntries();
}
