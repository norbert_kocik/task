package org.nk.repository;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import org.nk.model.Entry;


import javax.annotation.Nullable;
import java.util.*;

/**
 * Created by b on 03.05.15.
 */
public class MergingFeedsRepository implements EntryRepository {

    public interface MergingStrategy extends Function<Collection<Entry>,Entry> {
        //merge ->apply
    }

    public interface SameEntriesGrouper extends Function<Collection<Entry>,Collection<Collection<Entry>>> {
        //group ->apply
    }

    public static class NoGroupingSameEntriesGrouper implements SameEntriesGrouper {

        public Collection<Collection<Entry>> apply(Collection<Entry> entries) {
            return Collections2.transform(entries, new Function<Entry, Collection<Entry>>() {

                public Collection<Entry> apply(Entry entry) {
                    return Collections.singleton(entry);
                }
            });
        }
    }

    public static class GetFirstMergingStrategy implements MergingStrategy {

        public Entry apply(Collection<Entry> entries) {
            return entries.iterator().next();
        }
    }

    private List<EntryRepository> repositories;
    private MergingStrategy mergingStrategy;
    private SameEntriesGrouper sameEntriesGrouper;

    public MergingFeedsRepository(List<EntryRepository> repositories, SameEntriesGrouper sameEntriesGrouper, MergingStrategy mergingStrategy) {
        this.repositories = repositories;
        this.mergingStrategy = mergingStrategy;
        this.sameEntriesGrouper = sameEntriesGrouper;
    }

    private Collection<Entry> getEntriesWithDuplicates() {
        List<Entry> ret=new ArrayList<Entry>();
        for(EntryRepository repository:repositories) {
            ret.addAll(Arrays.asList(repository.getEntries()));//try/catch @ Log
        }
        return ret;
    }

    public Entry[] getEntries() {
        Collection<Entry> allEntries=getEntriesWithDuplicates();
        Collection<Collection<Entry>> sameEntries=sameEntriesGrouper.apply(allEntries);
        Collection<Entry> distinctEntries = Collections2.transform(sameEntries, mergingStrategy);
        return distinctEntries.toArray(new Entry[distinctEntries.size()]);
    }
}
