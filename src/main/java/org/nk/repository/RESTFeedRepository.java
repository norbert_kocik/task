package org.nk.repository;

import org.glassfish.jersey.client.JerseyClient;
import org.glassfish.jersey.client.JerseyClientBuilder;
import org.nk.model.Entry;

import javax.ws.rs.client.Client;
import java.net.URI;
import java.net.URL;

/**
 * Created by b on 03.05.15.
 */
public class RESTFeedRepository implements EntryRepository {

    private String/*URL*/ restURL;

    // we could do more parametrization

    public RESTFeedRepository(String restURL) {
        this.restURL = restURL;
    }


    public Entry[] getEntries() {
        Client client=JerseyClientBuilder.createClient();
        Entry[] ret = client.target(restURL).request().buildGet().invoke().readEntity(Entry[].class);
        if(ret==null) {
            return new Entry[0];
        }
        return ret;
    }
}
