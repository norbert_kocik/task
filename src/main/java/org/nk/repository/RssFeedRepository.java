package org.nk.repository;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import org.nk.model.Entry;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by b on 03.05.15.
 */
public class RssFeedRepository implements EntryRepository {

    //serialization helper class
    public class Rss {
        public Channel channel;
    }
    //serialization helper class
    public class Channel {
        public List<Entry> item;
    }

    private URL rssURL;

    public RssFeedRepository(URL rssURL) {
        this.rssURL = rssURL;
    }

    /*package*/ XStream createDeserialization() {
        XStream xstream = new XStream(new StaxDriver());
        xstream.ignoreUnknownElements();
        xstream.addImplicitCollection(Channel.class, "item", "item", Entry.class);
        xstream.alias("item", Entry.class);
        xstream.alias("rss", Rss.class);
        xstream.alias("channel", Channel.class);
        return xstream;
    }

    public Entry[] getEntries() {
        Rss rss=(Rss)createDeserialization().fromXML(rssURL);
        List<Entry> items=rss.channel.item;
        if(items==null) {
            return new Entry[0];
        }
        return items.toArray(new Entry[items.size()]);
    }
}
