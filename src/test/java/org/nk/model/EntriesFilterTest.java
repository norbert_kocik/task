package org.nk.model;

import com.google.common.base.Optional;
import org.fest.assertions.Assert;
import org.fest.assertions.Assertions;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

/**
 * Created by b on 03.05.15.
 */
public class EntriesFilterTest {

    @Test
    public void shouldFilterOutCategories() {
        Entry cat1=EntryBuilder.createByTitleAndCategory("a1", "cat1");
        Entry cat2=EntryBuilder.createByTitleAndCategory("a2","cat2");

        EntriesFilter underTest=new EntriesFilter(Optional.of("cat1"));
        Collection<Entry> filtered=underTest.apply(Arrays.asList(cat1, cat2));

        Assertions.assertThat(filtered).containsOnly(cat1);
    }
}