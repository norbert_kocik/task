package org.nk.model;

import static org.junit.Assert.*;

/**
 * Created by b on 03.05.15.
 * TODO: Make a normal builder
 */
public class EntryBuilder/*CreationHelper*/ {
    public static Entry createByTitle(String title) {
        return createByTitleAndCategory(title,null);
    }

    public static Entry createByTitleAndCategory(String title,String category) {
        return new Entry(title,null,null,null,null,category);
    }
}