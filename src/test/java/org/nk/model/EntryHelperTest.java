package org.nk.model;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by b on 04.05.15.
 */
public class EntryHelperTest {

    @Test
    public void testConvertPubDateToTime() throws Exception {
        assertEquals("15:26",EntryHelper.convertPubDateToTime("Sun, 03 May 2015 17:26:39 +0200"));//utc
    }

    @Test
    public void testConvertPubDateToDate() throws Exception {
        assertEquals("03 May 2015",EntryHelper.convertPubDateToDate("Sun, 03 May 2015 17:26:39 +0200"));
    }
}