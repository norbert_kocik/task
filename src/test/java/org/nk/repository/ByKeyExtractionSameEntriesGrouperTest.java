package org.nk.repository;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.nk.model.Entry;
import org.nk.model.EntryBuilder;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by b on 03.05.15.
 */
public class ByKeyExtractionSameEntriesGrouperTest {

    @Test
    public void shouldGroupByKey() throws Exception {
        Entry a1=EntryBuilder.createByTitle("a");
        Entry b=EntryBuilder.createByTitle("b");
        Entry a2=EntryBuilder.createByTitle("a");

        List<Entry> testData=Arrays.asList(a1,b,a2);
        ByKeyExtractionSameEntriesGrouper<String> underTest=ByKeyExtractionSameEntriesGrouper.ByTitleGrouper;

        Collection<Collection<Entry>> collections=underTest.apply(testData);

        Assertions.assertThat(collections).containsOnly(Arrays.asList(a1,a2),Arrays.asList(b));
    }
}