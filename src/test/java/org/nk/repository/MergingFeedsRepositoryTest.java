package org.nk.repository;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.mockito.Mockito;
import org.nk.model.Entry;
import org.nk.model.EntryBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created by b on 03.05.15.
 */
public class MergingFeedsRepositoryTest {

    protected EntryRepository createRepository(Entry...entries) {
        EntryRepository ret=Mockito.mock(EntryRepository.class);
        when(ret.getEntries()).thenReturn(entries);
        return ret;
    }

    @Test
    public void shouldGetEntriesFromAllRepositories() throws Exception {
        Entry a1=EntryBuilder.createByTitle("a1");
        Entry a2=EntryBuilder.createByTitle("a2");
        Entry a3=EntryBuilder.createByTitle("a3");
        List<EntryRepository> repositories=new ArrayList<EntryRepository>();
        repositories.add(createRepository(a1));
        repositories.add(createRepository(a3,a2));
        repositories.add(createRepository());

        MergingFeedsRepository underTest=new MergingFeedsRepository(repositories,
                new MergingFeedsRepository.NoGroupingSameEntriesGrouper(),
                new MergingFeedsRepository.GetFirstMergingStrategy());

        Entry[] entries=underTest.getEntries();

        Assertions.assertThat(Arrays.asList(entries)).containsOnly(a1,a2,a3);
    }
}