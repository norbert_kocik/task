package org.nk.repository;

import org.junit.Ignore;
import org.nk.model.Entry;

import java.net.URI;

import static org.junit.Assert.*;

/**
 * Created by b on 03.05.15.
 */
public class RESTFeedRepositoryTest {

    @Ignore //on the other hand class under test is dummy jersey client
    @org.junit.Test
    public void shouldGetEntries() throws Exception {
        String testURL="http://rexxars.com/playground/testfeed/";
        //wont work file://../src/test/resources/feeds/json/sample.json
        RESTFeedRepository underTest=new RESTFeedRepository(testURL);
        Entry[] entries=underTest.getEntries();
        assertTrue(entries.length>0);//etc
    }
}