package org.nk.repository;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import org.junit.Test;
import org.nk.model.Entry;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by b on 03.05.15.
 */
public class RssFeedRepositoryTest {

    @Test
    public void shouldSerializeSampleData() throws MalformedURLException, FileNotFoundException {
        RssFeedRepository underTest=new RssFeedRepository(new URL("http://localhost"));
        RssFeedRepository.Rss rss= (RssFeedRepository.Rss)underTest.createDeserialization().fromXML(new FileReader("./src/test/resources/feeds/rss/sample.xml"));
        List<Entry> items=rss.channel.item;
        assertTrue(items.size() > 0);//etc
        assertEquals(items.get(0).getTitle(), "Suverene Chelsea sikret seriegullet");
        assertEquals(items.get(4).getTitle(), "Utrolig stunt");
    }

    @Test
    public void shouldSerializeEmptyData() throws MalformedURLException, FileNotFoundException {
        RssFeedRepository underTest=new RssFeedRepository(new URL("http://localhost"));
        RssFeedRepository.Rss rss= (RssFeedRepository.Rss)underTest.createDeserialization().fromXML(new FileReader("./src/test/resources/feeds/rss/empty.xml"));
        List<Entry> items=rss.channel.item;
        assertNull(items);//info for making getEntries()!=null
    }

    // core moved to shouldSerializeXYZData methods
    @Test
    public void testGetEntries() throws Exception {

        URL url=new URL("http://www.vg.no/rss/feed/forsiden/?frontId=1");
        RssFeedRepository underTest=new RssFeedRepository(url);
        Entry[] items= underTest.getEntries();

        assertTrue(items.length > 0);//etc
    }
}